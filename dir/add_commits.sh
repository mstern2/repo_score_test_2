# Create or modify a file 100 times and commit each change
for i in {1..100}
do
   echo "This is commit number $i" >> commit_log.txt
   git add commit_log.txt
   git commit -m "Commit number $i"
   #sleep 1  # Introduce a small delay to ensure each commit is processed
done

# Push the commits to the remote repository
#git push origin main  # or the branch you are working onertert